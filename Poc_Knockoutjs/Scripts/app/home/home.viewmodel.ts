﻿

class RequestStatistic {
    name: string;
    requests: Array<number>;

    constructor(name) {
        this.name = name;
        this.requests = new Array<number>();
    }
}

class HomeViewModel {
    service : HomeService;
    chart: HighchartsChartObject;
    statistic: KnockoutObservableArray<RequestStatistic>;

    paintNewValues = (values :RequestStatistic[]) => {
        for (var i = 0; i < this.chart.series.length; i++) {
            while (this.chart.series.length > 0) {
                this.chart.series[0].remove(true);
            }

            ko.utils.arrayForEach(values, (stat, index) => {

                var serieOptions: HighchartsIndividualSeriesOptions = <HighchartsIndividualSeriesOptions>{};

                serieOptions.name = stat.name;
                serieOptions.data = stat.requests;

                this.chart.addSeries(serieOptions);

            });
        };
    }

    

    load = () => {

        this.service.getLastYearStatitics()
            .done(data => {
                var list = new Array<RequestStatistic>();

                ko.utils.arrayForEach(data, item => {
                    var one = new RequestStatistic(item.Name);
                    item.Requests.forEach(n => {
                        one.requests.push(n);
                    });

                    list.push(one);
                });

                this.statistic(list);

            });

    }

    createChart() {
        var charOptions: HighchartsOptions = {
            title: {
                text: 'Last year request',
                x: -20 //center
            },
            xAxis: {
                categories: [
                    'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                ]
            },
            yAxis: {
                title: {
                    text: 'Request'
                },
                plotLines: [
                    {
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }
                ]
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            chart: {
                renderTo: "containerChart1"
            },
            
            series: [{}]
        };


        this.chart = new Highcharts.Chart(charOptions);
        
    }

    constructor(service: HomeService) {
        this.service = service;
        this.createChart();
        this.statistic = ko.observableArray<RequestStatistic>();
        this.statistic.subscribe(values => {
            this.paintNewValues(values);
            toastr.success("chart data loaded");
        });

        this.load();
    }
}