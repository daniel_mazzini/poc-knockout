var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var LoggerService = (function (_super) {
    __extends(LoggerService, _super);
    function LoggerService() {
        var _this = this;
        _super.apply(this, arguments);
        this.getFilterLog = function (pageNumber) {
            return _this.getJson("api/bo/AuditLog?$count=true&$skip=" + (pageNumber - 1) * 10);
        };
        this.getCountLog = function (correlationId) {
            return _this.getJson("api/log/count?correlationId=" + correlationId);
        };
    }
    return LoggerService;
})(ServiceBase);
//# sourceMappingURL=loggerService.js.map