﻿class LoggerService extends ServiceBase {
    
    public getFilterLog = (pageNumber: number): JQueryPromise<any>=> {
        return this.getJson<any>("api/bo/AuditLog?$count=true&$skip=" + (pageNumber - 1) * 10);
    }

    public getCountLog = (correlationId: string): JQueryPromise<number> => {
        return this.getJson<number>("api/log/count?correlationId=" + correlationId);
    }
}