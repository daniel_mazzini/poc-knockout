var LogGridItemModel = (function () {
    function LogGridItemModel() {
    }
    return LogGridItemModel;
})();
var ODataCountResult = (function () {
    function ODataCountResult(data) {
        this.count = data["@odata.count"];
        this.list = data["value"];
    }
    return ODataCountResult;
})();
//# sourceMappingURL=LogGridItemModel.js.map