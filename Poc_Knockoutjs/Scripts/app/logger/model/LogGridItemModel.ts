﻿class LogGridItemModel {
    id: number;
    correlationId: string;
    timeCost : number;
    requestLogDateTime: string;
    requestHeaderVersion: string;
    requestUrl: string;
    requestCountry: string;
    requestToken: string;
    requestBody: string;
    responseDateTime: string;
    responseHttpStatusCode: number;
    responseBody: string;
}



class ODataCountResult<TResult> {
    count: number;
    list : Array<TResult>;

    constructor(data: Object) {
        this.count = <number>data["@odata.count"];
        this.list = <TResult[]>data["value"];
    }


}