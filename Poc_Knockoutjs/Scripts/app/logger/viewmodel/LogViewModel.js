var LogViewModel = (function () {
    function LogViewModel(logService) {
        var _this = this;
        this.filter = function () {
            _this.logService.getFilterLog("algo")
                .done(function (data) {
                // map
                var newData = ko.utils.arrayMap(data, function (item) {
                    var rtv = new LogGridItemModel();
                    rtv.requestDateTime = item.requestDateTime;
                    return rtv;
                });
                //dataBind
                _this.logItems(newData);
            })
                .fail();
        };
        this.logService = logService;
        this.logItems = ko.observableArray();
    }
    return LogViewModel;
})();
