var LoggerViewModel = (function () {
    function LoggerViewModel(logService) {
        var _this = this;
        this.beginFilter = function () {
            _this.loading(true);
            _this.numberOfEntries(0);
            _this.pageNumber(1);
            _this.logService.getCountLog(_this.correlationId)
                .done(function (data) {
                _this.numberOfEntries(data);
                _this.doFilter();
            })
                .fail(function (errorData) {
                toastr.error(errorData);
                _this.loading(false);
            });
        };
        this.canSearchPrevious = function () {
            return (_this.pageNumber() > 1);
        };
        this.canSearchNext = function () {
            var record = _this.pageNumber() * 10;
            return (record < _this.numberOfEntries());
        };
        this.searchNext = function () {
            _this.loading(true);
            _this.pageNumber(_this.pageNumber() + 1);
            _this.doFilter();
        };
        this.searchPrevious = function () {
            _this.loading(true);
            _this.pageNumber(_this.pageNumber() - 1);
            _this.doFilter();
        };
        this.doFilter = function () {
            _this.logService.getFilterLog(_this.pageNumber())
                .done(function (data) {
                var odata = new ODataCountResult(data);
                // map
                var newData = ko.utils.arrayMap(odata.list, function (item) {
                    var rtv = new LogGridItemModel();
                    rtv.id = item.Id;
                    rtv.correlationId = item.CorrelationId;
                    rtv.requestLogDateTime = item.RequestLogDateTime;
                    rtv.requestBody = item.RequestBody;
                    rtv.requestCountry = item.RequestCountry;
                    rtv.requestToken = item.RequestToken;
                    rtv.requestHeaderVersion = item.RequestHeaderVersion;
                    rtv.responseBody = item.ResponseBody;
                    rtv.responseDateTime = item.ResponseDateTime;
                    rtv.responseHttpStatusCode = item.ResponseHttpStatusCode;
                    rtv.timeCost = item.TimeCost;
                    return rtv;
                });
                //dataBind
                _this.logItems(newData);
                _this.numberOfEntries(odata.count);
            })
                .fail(function (errorData) {
                toastr.error(errorData);
            }).always(function (d) {
                _this.loading(false);
            });
        };
        this.logService = logService;
        this.loading = ko.observable(false);
        this.logItems = ko.observableArray();
        this.numberOfEntries = ko.observable(0);
        this.pageNumber = ko.observable(1);
        this.correlationId = "49EEF3BC-1630-4047-8B5C-E7C016B29203";
    }
    return LoggerViewModel;
})();
//# sourceMappingURL=loggerListViewModel.js.map