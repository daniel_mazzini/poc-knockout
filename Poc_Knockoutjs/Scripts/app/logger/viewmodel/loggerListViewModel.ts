﻿class LoggerViewModel {
    logService: LoggerService;
    loading: KnockoutObservable<boolean>;
    logItems : KnockoutObservableArray<LogGridItemModel>;
    numberOfEntries: KnockoutObservable<number>;
    pageNumber: KnockoutObservable<number>;
    correlationId: string;

    constructor(logService: LoggerService) {
        this.logService = logService;
        this.loading = ko.observable<boolean>(false);
        this.logItems = ko.observableArray<LogGridItemModel>();
        this.numberOfEntries = ko.observable(0);
        this.pageNumber = ko.observable(1);
        this.correlationId = "49EEF3BC-1630-4047-8B5C-E7C016B29203";
    }

    public beginFilter = () => {

        this.loading(true);
        this.numberOfEntries(0);
        this.pageNumber(1);

        this.doFilter();

    }

    private canSearchPrevious = (): boolean => {
        return (this.pageNumber() > 1);
    }

    private canSearchNext = (): boolean => {
        var record = this.pageNumber() * 10;
        return (record < this.numberOfEntries());
    }

    public searchNext = () => {
        this.loading(true);
        this.pageNumber(this.pageNumber() + 1);
        this.doFilter();
    }

    public searchPrevious = () => {
        this.loading(true);
        this.pageNumber(this.pageNumber() - 1);
        this.doFilter();
    }
    private doFilter = () => {

        this.logService.getFilterLog(this.pageNumber())
            .done(data => {

                var odata: ODataCountResult<T4TS.LoggerDataGridItem> = new ODataCountResult<T4TS.LoggerDataGridItem>(data); 

                // map
                var newData = ko.utils.arrayMap<T4TS.LoggerDataGridItem, LogGridItemModel>(odata.list, (item) => {
                    var rtv = new LogGridItemModel();
                    rtv.id = item.Id;
                    rtv.correlationId = item.CorrelationId;
                    rtv.requestLogDateTime = item.RequestLogDateTime;
                    rtv.requestBody = item.RequestBody;
                    rtv.requestCountry = item.RequestCountry;
                    rtv.requestToken = item.RequestToken;
                    rtv.requestHeaderVersion = item.RequestHeaderVersion;
                    rtv.responseBody = item.ResponseBody;
                    rtv.responseDateTime = item.ResponseDateTime;
                    rtv.responseHttpStatusCode = item.ResponseHttpStatusCode;
                    rtv.timeCost = item.TimeCost;
                    return rtv;
                });

                //dataBind
                this.logItems(newData);
                this.numberOfEntries(odata.count);

            })
            .fail((errorData) => {
                toastr.error(errorData);
            }).always(d => {
                this.loading(false);
            });
    }

}