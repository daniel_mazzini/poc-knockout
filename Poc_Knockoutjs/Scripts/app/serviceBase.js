/// <reference path="../typings/jquery/jquery.d.ts" />
var ServiceBase = (function () {
    function ServiceBase(baseurl) {
        this.baseUrl = baseurl;
    }
    ServiceBase.prototype.getJson = function (url) {
        return this.callJson("GET", url, null);
    };
    ServiceBase.prototype.postJson = function (url, data) {
        return this.callJson("POST", url, data);
    };
    ServiceBase.prototype.callJson = function (method, url, data) {
        var deferred = $.Deferred();
        var options = {
            type: method,
            url: this.baseUrl + url,
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        };
        if (data) {
            options.data = ko.toJSON(data);
        }
        $.ajax(options)
            .done(function (data) {
            deferred.resolve(data);
        })
            .fail(function (data) {
            deferred.reject(data);
        });
        return deferred.promise();
    };
    return ServiceBase;
})();
//# sourceMappingURL=serviceBase.js.map