﻿/// <reference path="../typings/jquery/jquery.d.ts" />

declare var gWebApiToken: string;
declare var gCurrentCulture: string;
declare var gCurrentCultureShort: string;

class ServiceBase {

    private baseUrl: string;

    constructor(baseurl: string) {
        this.baseUrl = baseurl;
    }

    protected  getJson<TResponse>(url: string): JQueryPromise<TResponse> {
        return this.callJson<TResponse, any>("GET", url, null);
    }

    postJson<TResponse, TRequest>(url: string, data: TRequest) {
        return this.callJson("POST", url, data);
    }

    private callJson<TResponse, TRequest>(method: string, url: string, data: TRequest): JQueryPromise<TResponse> {

        var deferred = $.Deferred<TResponse>();

        var options: JQueryAjaxSettings = {
            type: method,
            url: this.baseUrl + url,
            contentType: "application/json; charset=utf-8",
            dataType: "json"
            //headers: {
            //    'Authorization': "Bearer " + gWebApiToken,
            //    'Accept-Language': [gCurrentCulture]
            //},
        };

        if (data) {
            options.data = ko.toJSON(data);
        }

        $.ajax(options)
            .done(data => {
                deferred.resolve(<TResponse>data);
            })
            .fail(data => {
                deferred.reject(data);
            });

        return deferred.promise();

    }
}