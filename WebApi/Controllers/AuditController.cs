using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.OData;
using System.Web.OData.Query;
using WebApi.Contexto;

namespace WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AuditLogController : ODataController
    {
        private readonly LogData _loggReader = new LogData();

        [EnableQuery(PageSize = 10, AllowedQueryOptions = AllowedQueryOptions.All)]
        public IQueryable<AuditLog> Get()
        {
            return this._loggReader.AuditLogs;
        }

        [EnableQuery]
        public SingleResult<AuditLog> Get(int key)
        {
            IQueryable<AuditLog> result = this._loggReader.AuditLogs.Where(p => p.Id == key);
            return SingleResult.Create(result);
        }

        /// <summary>
        /// Releases the unmanaged resources that are used by the object and, optionally, releases the managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            this._loggReader.Dispose();
            base.Dispose(disposing);
        }
    }
}