using System;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.Models;

namespace WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LogController : ApiController
    {
        private const int PageSize = 50;
        public IHttpActionResult Get(Guid correlationId, int pageNumber = 1)
        {
            var ahora = DateTime.UtcNow.AddDays(-5);
            var skipRecords = (pageNumber - 1) * PageSize;

            var list =  Enumerable.Range(0, 100)
                .Select(index => new LoggerDataGridItem()
                {
                    Id = index,
                    CorrelationId = correlationId,
                    RequestBody = "Body" + index.ToString(),
                    RequestHeaderVersion = "octov1",
                    ResponseBody = "response" + index.ToString(),
                    ResponseDateTime = ahora.AddHours(index).AddMilliseconds(index),
                    RequestCountry = "ch",
                    RequestLogDateTime = ahora.AddHours(index),
                    RequestToken = "token",
                    RequestUrl = "Login",
                    ResponseHttpStatusCode = 200,
                    TimeCost = index
                })
                .Skip(skipRecords)
                .Take(PageSize);


            return Ok(list);

        }

        [HttpGet]
        [Route("api/log/count")]
        public IHttpActionResult Count(Guid correlationId)
        {
            return Ok(100);
        }
    }
}