﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.Models;

namespace WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class StatisticController : ApiController
    {
        public IHttpActionResult Get()
        {
            var requestLogin = new RequestStatistic
            {
                Name = "Login",
                Requests = new List<double>() { 70, 69, 95, 110, 130, 150, 160, 145, 182, 215, 222, 235 }
            };

            var requestTrip = new RequestStatistic
            {
                Name = "Trip",
                Requests = new List<double>() { 140, 138, 190, 220, 260, 300, 320, 290, 364, 430, 444, 470 }
            };

            var requestTripDetails = new RequestStatistic
            {
                Name = "TripDetail",
                Requests = new List<double>() { 84, 85, 114, 132, 156, 180, 192, 174, 218, 258, 266, 282 }
            };

            List<RequestStatistic> response = new List<RequestStatistic> { requestLogin, requestTrip, requestTripDetails };

            return this.Ok(response);
        }
    }
}
