﻿using System;
using T4TS;

namespace WebApi.Models
{
    [TypeScriptInterface]
    public class LoggerDataGridItem
    {
        public int Id { get; set; }

        public Guid CorrelationId { get; set; }

        public int TimeCost { get; set; }

        public DateTime RequestLogDateTime { get; set; }

        public string RequestHeaderVersion { get; set; }

        public string RequestUrl { get; set; }

        public string RequestCountry { get; set; }

        public string RequestToken { get; set; }

        public string RequestBody { get; set; }

        public DateTime ResponseDateTime { get; set; }

        public int ResponseHttpStatusCode { get; set; }

        public string ResponseBody { get; set; }

    }
}