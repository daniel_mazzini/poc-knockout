﻿using System;
using System.Collections.Generic;
using T4TS;

namespace WebApi.Models
{
    [T4TS.TypeScriptInterface]
    public class RequestStatistic
    {
        public RequestStatistic()
        {
            this.Requests = new List<Double>();
        }
        public string Name { get; set; }
        public List<Double> Requests { get; set; }
    }
}